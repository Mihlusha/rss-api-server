import psycopg2
from psycopg2 import Error


def auth() -> list:
    with open("data_auth.txt", "r", encoding="utf-8") as f:
        i = f.readline().split()
    return i


host = auth()[0]
user = auth()[1]
password = auth()[2]
db_name = auth()[3]
port = auth()[4]


db = psycopg2.connect(
    user=user,
    # пароль, который указали при установке PostgreSQL
    password=password,
    host=host,
    port=port,
    database=db_name
)
sql = db.cursor()

create_client = """
CREATE TABLE IF NOT EXISTS client (
    ID SERIAL PRIMARY KEY,
    phone_client VARCHAR(12),
    code_phone_client VARCHAR(4),
    teg_client TEXT,
    time_zone_client TEXT,
    amount_msg_client INT
    
    
    

);
"""

creat_rss = """
CREATE TABLE IF NOT EXISTS rss (
    ID SERIAL PRIMARY KEY,
    time_start_rss TEXT,
    msg_rss TEXT,
    filter_rss TEXT,
    time_stop TEXT,
    stat_rss TEXT,
    amount_msg_rss INT
    
);
"""

creat_msg = """
CREATE TABLE IF NOT EXISTS msg (
    ID SERIAL PRIMARY KEY,
    text_msg TEXT,
    time_send_msg TEXT,
    status_send_msg TEXT,
    id_rss TEXT,
    id_client TEXT,
    stat_msg TEXT
);
"""



def creat_conection():
    try:
        sql.execute(create_client)
        sql.execute(creat_rss)
        sql.execute(creat_msg)
        db.commit()
    except Error as e:
        print(f'Родилась ошибка {e}')
        raise e


def write_sql(sett, value):
    sql.execute(f"""INSERT INTO users ('id') VALUES (?)""", (value,))
    db.commit()


creat_conection()

# write_sql('id', '2343')

# for value in sql.execute("SELECT * FROM users WHERE id = '2343' "):
#     print(value)

# sql.execute(f"UPDATE users SET id = '777' WHERE id = '2343'")
# db.commit()
# for value in sql.execute("SELECT * FROM users WHERE id = '777' "):
#     print(value)