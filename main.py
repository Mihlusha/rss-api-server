import json

from db import sql, db
import time
import traceback
import threading as tr
import requests
import json



def database_requests(action: str, body: tuple) -> str:
    # Инструкции для функции:
#action это строка с определением действия:
#    'UPDATE CONCAT' - добавление значений к уже имеющимся
#    'UPDATE' - обновление имеющегося значения
#    'SELECT' - считывание данных из базы
#    'INSERT' - создание новой записи
#body это кортеж из набора параметров для работы с базой
#Параметры вписываются следующим образом:
# если action == 'UPDATE CONCAT', то
# [0] - таблица в которой делаем изменение,
# [1] - параметр для добавления,
# [2] - значение для добавления,
# [3] - id для индетефикации

# если action == 'UPDATE', то
# [0] - таблица в которой делаем изменение,
# [1] - параметр для назначения,
# [2] - значение для параметра,
# [3] - id для индетефикации

# если action == 'SELECT', то
# [0] - параметр для выборки,
# [1] - таблица в которой делаем выборку,
# [2] - id для индетефикации

# если action == 'INSERT', то
# [0] - таблица для создания записи,
# [1] - столбцы которые будем заполнять,
# [2] - значения для заполняемых столбцов

    if action == "SELECT":
        activ = True
        while activ:
            try:
                sql.execute(f"SELECT {body[0]} "
                            f"FROM {body[1]} "
                            f"WHERE id = '{body[2]}'")
                resp = sql.fetchall()[0][0]

                if resp is None:
                    pass
                else:
                    activ = False
                    return resp

            except BaseException:
                time.sleep(3)

    elif action == "UPDATE":
        sql.execute(f"UPDATE {body[0]} "
                    f"SET {body[1]} = {body[2]}  "
                    f"WHERE id = '{body[3]}'")
        db.commit()

    elif action == "UPDATE CONCAT":
        sql.execute(
            f"UPDATE {body[0]} "
            f"SET {body[1]} = CONCAT(NULLIF({body[1]},''),'{body[2]} ') "
            f"WHERE id = '{body[3]}'")
        db.commit()

    elif action == "INSERT":
        sql.execute(f"""INSERT INTO {body[0]} ({body[1]}) 
                        VALUES ({body[2]}) RETURNING ID""")
        db.commit()
        id = sql.fetchone()[0]
        return id


def check_activ_rss():
    list_activ_rss = []
    while True:
        time.sleep(1)

        sql.execute(f"SELECT * "
                    f"FROM rss ")
        tuple_activ_rss = sql.fetchall()

        for i in tuple_activ_rss:
            try:
                if int(i[1]) < int(time.time()) and int(i[4]) > int(time.time()):
                    if i[0] not in list_activ_rss:
                        sql.execute(f"SELECT id "
                                    f"FROM msg "
                                    f"WHERE id_rss = '{i[0]}'")
                        msg_id = sql.fetchone()[0]
                        tr.Thread(target=rss_procces, args=(i[0],i[2],i[3],i[4],msg_id,)).start() # args = (id_rss,message,
                                                                                         # filter_rss,time_end_rss,msg_id)
                        list_activ_rss.append(i[0])
            except:
                pass





def rss_procces(id_rss,message,filter_rss, time_end_rss,msg_id):
    filter_tag = filter_rss[filter_rss.find('tag')+4:filter_rss.find('code')-1].split(',') if filter_rss.find('tag') \
                                                                                              != -1 else []
    filter_code = filter_rss[filter_rss.find('code')+5:].split(',')
    print(filter_tag)
    print(filter_code)
    activ = True
    while activ:
        time.sleep(2)
        print('Chek clients ///...///')
        try:
            sql.execute(f"SELECT * "
                        f"FROM client "
                        )
            clients = sql.fetchall()
            print(clients)
            if clients == []:
                pass
            else:
                activ = False

        except BaseException:
            time.sleep(3)

    for i in clients:
        phone_client = i[1]
        phone_code_client = i[2].split(',')
        tag_client = i[3].split(',')
        time_zone_client = i[4]
        if int(time_end_rss) > int(time.time()) and any(x in phone_code_client for x in filter_code) or \
                any(x in tag_client for x in filter_tag):
            pre_msg_send = {'id': int(msg_id), 'phone': int(phone_client), 'text': str(message)}
            msg_send = json.dumps(pre_msg_send)
            print(f'Отправили сообщение: {message}\nНа номер {phone_client}')
            headers = {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTE0ODg3OTEsImlzc'
                                        'yI6ImZhYnJpcXVlIiwibmFtZSI6Im5hZ25hbG92In0.25no_s_tPhbrJ-bEG'
                                        'RzVT5zPXhIMQwX8igEFkuaI9GU',
                       'Content-Type':'application/json',
                       'accept':'application/json',
            }
            err = True
            while err:
                try:
                    r = requests.post(f"http://probe.fbrq.cloud/v1/send/{msg_id}", headers=headers, data=msg_send)
                    if str(r) == '<Response [200]>':
                        print('Успешно доставлено')
                        database_requests('UPDATE', ('rss','amount_msg_rss','amount_msg_rss + 1',id_rss))
                    elif str(r) == '<Response [400]>':
                        print('Ошибка в запросе')
                    elif str(r) == '<Response [401]>':
                        print('Ошибка авторизации')
                    err = False
                except Exception as er:
                    print(traceback.format_exc())
                    print(er)
                    time.sleep(3)
                    pass

#rss_procces(1,"privet piska","code_900,953",1660087063)
check_activ_rss()


