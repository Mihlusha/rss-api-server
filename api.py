from flask import Flask, jsonify
from db import sql, db
import time
import threading as tr

app = Flask(__name__)

def database_requests(action: str, body: tuple) -> str:
    # Инструкции для функции:
#action это строка с определением действия:
#    'UPDATE CONCAT' - добавление значений к уже имеющимся
#    'UPDATE' - обновление имеющегося значения
#    'SELECT' - считывание данных из базы
#    'INSERT' - создание новой записи
#body это кортеж из набора параметров для работы с базой
#Параметры вписываются следующим образом:
# если action == 'UPDATE CONCAT', то
# [0] - таблица в которой делаем изменение,
# [1] - параметр для добавления,
# [2] - значение для добавления,
# [3] - id для индетефикации

# если action == 'UPDATE', то
# [0] - таблица в которой делаем изменение,
# [1] - параметр для назначения,
# [2] - значение для параметра,
# [3] - id для индетефикации

# если action == 'SELECT', то
# [0] - параметр для выборки,
# [1] - таблица в которой делаем выборку,
# [2] - id для индетефикации

# если action == 'INSERT', то
# [0] - таблица для создания записи,
# [1] - столбцы которые будем заполнять,
# [2] - значения для заполняемых столбцов

    if action == "SELECT":
        activ = True
        while activ:
            try:
                sql.execute(f"SELECT {body[0]} "
                            f"FROM {body[1]} "
                            f"WHERE id = '{body[2]}'")
                resp = sql.fetchall()[0][0]

                if resp is None:
                    pass
                else:
                    activ = False
                    return resp

            except BaseException:
                time.sleep(3)

    elif action == "UPDATE":
        sql.execute(f"UPDATE {body[0]} "
                    f"SET {body[1]} = {body[2]}  "
                    f"WHERE id = '{body[3]}'")
        db.commit()

    elif action == "UPDATE CONCAT":
        sql.execute(
            f"UPDATE {body[0]} "
            f"SET {body[1]} = CONCAT(NULLIF({body[1]},''),'{body[2]} ') "
            f"WHERE id = '{body[3]}'")
        db.commit()

    elif action == "INSERT":
        sql.execute(f"""INSERT INTO {body[0]} ({body[1]}) 
                        VALUES ({body[2]}) RETURNING ID""")
        db.commit()
        id = sql.fetchone()[0]
        return id

@app.route('/add_client/<phone_client>/<teg_client>/<time_zone>', methods=['GET'])
#записываем данные о клиенте и возвращаем его id
def add_client(phone_client, teg_client, time_zone):
    code_phone_client = phone_client[1:4]
    id = database_requests('INSERT',('client','phone_client, code_phone_client, teg_client, time_zone_client',
                                f"{phone_client}, {code_phone_client}, '{teg_client}', '{time_zone}'",))
    print(id)
    return jsonify({'id': id})

@app.route('/update_client/<id_client>/<phone_client>/<teg_client>/<time_zone>', methods=['GET'])
#Переписыавем данные о клиенте зная его id, если id нет, то возвращаем сообщение о том, что id не найден и
# предлагаем воспользоваться командой add_client
def update_client(id_client,phone_client, teg_client, time_zone):
    code_phone_client = phone_client[1:4]

    sql.execute(f"UPDATE client "
                f"SET phone_client = {phone_client},  "
                f"code_phone_client = {code_phone_client},  "
                f"teg_client = {teg_client},  "
                f"time_zone_client = {time_zone}  "
                f"WHERE ID = {id_client}" )
    db.commit()
    return jsonify({'status': 'Ok'})

@app.route('/delete_client/<id_client>', methods=['GET'])
#Удаляем запись о клиенте по его id
def delete_client(id_client):
    sql.execute(f"DELETE FROM client WHERE ID = {id_client}")
    db.commit()
    return jsonify({'status': 'Ok'})

@app.route('/add_rss/<time_start>/<time_stop>/<filter_tag>/<filter_code>/<message>', methods=['GET'])
#создаем новую рассылку и возвращаем id_rss
def add_rss(time_start, time_stop, filter_tag, filter_code, message):
    filter_tag = f"tag_{filter_tag}"
    filter_code = f"code_{filter_code}"
    filter_rss = f"{filter_tag} {filter_code}"
    id = database_requests('INSERT',('rss','time_start_rss, time_stop, filter_rss, msg_rss, amount_msg_rss',
                                f"{time_start}, {time_stop}, '{filter_rss}', '{message}', {int(0)}",))
    print(id)

    database_requests('INSERT', ('msg','id_rss, text_msg',f"{id}, '{message}'",))
    return jsonify({'id': id})

@app.route('/update_rss/<id_rss>/<time_start>/<time_stop>/<filter_tag>/<filter_code>/<message>', methods=['GET'])
#Переписыавем данные о рассылке зная ее id, если id нет, то возвращаем сообщение о том, что id не найден
#и предлагаем воспользоваться командой add_rss
def update_rss(id_rss,time_start, time_stop, filter_tag, filter_code, message):
    amount_msg_rss = database_requests('SELECT',('amount_msg_rss','rss',id_rss))
    sql.execute(f"DELETE FROM rss WHERE ID = {id_rss}")
    db.commit()
    filter_tag = f"tag_{filter_tag}"
    filter_code = f"code_{filter_code}"
    filter_rss = f"{filter_tag} {filter_code}"
    id = database_requests('INSERT', ('rss', 'time_start_rss, time_stop, filter_rss, msg_rss, amount_msg_rss',
                                      f"{time_start}, {time_stop}, '{filter_rss}', '{message}', "
                                      f"{int(amount_msg_rss)}",))

    sql.execute(f"DELETE FROM msg WHERE id_rss = '{id_rss}'")
    db.commit()
    database_requests('INSERT', ('msg', 'id_rss, text_msg', f"{id}, '{message}'",))
    return jsonify({'id': id})

@app.route('/delete_rss/<id_rss>', methods=['GET'])
#Удаляем запись о рассылке по ее id
def delete_rss(id_rss):
    sql.execute(f"DELETE FROM rss WHERE ID = {id_rss}")
    db.commit()
    return jsonify({'status': 'Ok'})

@app.route('/active_rss', methods=['GET'])
#список активных подписок
def active_rss():
    list_id_activ_rss = []
    sql.execute(f"SELECT time_start_rss, time_stop, id "
                f"FROM rss ")
    tuple_activ_rss = sql.fetchall()
    for i in tuple_activ_rss:


        try:

            if int(i[0]) < int(time.time()) and int(i[1]) > int(time.time()):

                list_id_activ_rss.append(i[2])
        except:
            pass

    return jsonify({'active': list_id_activ_rss})
@app.route('/get_stat_rss/<id_rss>', methods=['GET'])
#статиска по конкретной рассылке
def get_stat_rss(id_rss):
    dict_stat_rss = {}
    sql.execute(f"SELECT amount_msg_rss, time_start_rss, time_stop, id "
                f"FROM rss WHERE id = {id_rss}")
    tuple_stat_rss = sql.fetchone()
    sql.execute(f"SELECT text_msg "
                f"FROM msg WHERE id_rss = '{id_rss}'")
    tuple_stat_msg = sql.fetchone()
    dict_stat_rss['Amount send message'] = tuple_stat_rss[0]
    dict_stat_rss['Text message'] = tuple_stat_msg[0]

    print(dict_stat_rss)
    return jsonify({'stat': dict_stat_rss})


@app.route('/get_stat_rss', methods=['GET'])
#статистика по всем рассылкам
def get_stat_rss_all():
    dict_stat = {}
    tuple_active_rss = ()
    sql.execute(f"SELECT amount_msg_rss, time_start_rss, time_stop, id "
                f"FROM rss")
    tuple_stat_rss = sql.fetchall()
    sql.execute(f"SELECT id "
                f"FROM client")
    tuple_client = sql.fetchall()
    amount_msg = 0
    for i in tuple_stat_rss:
        try:
            if int(i[1]) < int(time.time()) and int(i[2]) > int(time.time()):
                tuple_active_rss.append(i[3])
            amount_msg += i[0]

        except:
            pass
    dict_stat['Amount RSS'] = len(tuple_stat_rss)
    dict_stat['Active RSS'] = len(tuple_active_rss)
    dict_stat['Amount Message'] = amount_msg
    dict_stat['Amount Client'] = len(tuple_client)

    print(dict_stat)
    return jsonify({'stat': dict_stat})


@app.route('/get_msg', methods=['GET'])
#список сообщений с их id и text_msg
def get_msg():
    msg_dict = {}
    sql.execute(f"SELECT id, text_msg "
                f"FROM msg ")
    msg = sql.fetchall()
    for i in msg:
        msg_dict[i[0]] = i[1]
    print(msg_dict)
    return jsonify({'msg': msg_dict})




if __name__ == '__main__':
    app.run()